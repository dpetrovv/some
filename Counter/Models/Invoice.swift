//
//  Invoice.swift
//  Counter
//
//  Created by Denis on 01.01.2020.
//  Copyright © 2020 Handmade. All rights reserved.
//

final class Invoice: Codable {

    // MARK: - Properties

    var name: String
    let euroValuta: Valuta
    let rubblesValuta: Valuta

}

struct Valuta: Codable {

    private let currency: Currency
    private var value: Double = 0
}


enum Currency: String, Codable {
    case rubbles
    case euro
}
