//
//  CounterTableViewCell.swift
//  Counter
//
//  Created by Denis on 01.01.2020.
//  Copyright © 2020 Handmade. All rights reserved.
//

import UIKit

final class CounterTableViewCell: UITableViewCell {

    // MARK: - Properties

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var euroLabel: UILabel!
    @IBOutlet private weak var rubblesLabel: UILabel!

    // MARK: - Methods

    func configure() {
        
    }
}
